# FB analysis backend

This project analyses Channel42's posts after warehousing their data. To start warehousing, visit the root URL. To update existing posts, visit the same URL.

To run the server, do the following steps:

* Install rails 5.0.2

* Install redis and start the redis server

* In redis-cli, type this command: `SET access_token EAAFDgPBlyk8BAPWLDAI18JTN4re0PJpZA5CurPzUGHutXDHzrDCzH7hLOuZA1Nljjyl4q9zZCXaxR4FCvc63BEGNCy65YXzy5NQX2Up8eCqHs3QSfzEIKlhZAsshfyLhHw2JUgWTuoOuTLdVdryz0DBsCykPwIMZD`

* Install postgresql and start the server. Create a database. Enter the database credentials in database.yml (default fb_data)

* In project directory, execute `bundle install` to install required gems

* Run `bin/rails db:migrate` to create tables

* Start sidekiq server by executing `bundle exec sidekiq`

* In the project directory, execute `bin/rails s`

* Visit `http://localhost:3000` to begin warehousing.

Default port = 3000

