Sequel.migration do
  change do
    create_table(:posts) do
      column :id, "text", :null=>false
      column :created_time, "timestamp without time zone"
      column :shares, "integer"
      
      primary_key [:id]
    end
    
    create_table(:schema_migrations) do
      column :filename, "text", :null=>false
      
      primary_key [:filename]
    end
    
    create_table(:users) do
      column :id, "text", :null=>false
      column :likes, "integer", :default=>0, :null=>false
      column :comments, "integer", :default=>0, :null=>false
      
      primary_key [:id]
      
      index [:comments]
      index [:likes]
    end
    
    create_table(:comments) do
      column :id, "text", :null=>false
      foreign_key :user_id, :users, :type=>"character varying(255)", :key=>[:id]
      foreign_key :post_id, :posts, :type=>"character varying(255)", :key=>[:id]
      column :created_time, "timestamp without time zone"
      
      primary_key [:id]
    end
    
    create_table(:likes) do
      primary_key :id
      foreign_key :user_id, :users, :type=>"character varying(255)", :key=>[:id]
      foreign_key :post_id, :posts, :type=>"character varying(255)", :key=>[:id]
      
      index [:user_id, :post_id], :unique=>true
    end
    
    create_table(:user_month_visits) do
      primary_key :id
      column :visit_month, "date"
      foreign_key :user_id, :users, :type=>"character varying(255)", :key=>[:id]
      column :likes, "integer", :default=>0, :null=>false
      column :comments, "integer", :default=>0, :null=>false
      
      index [:comments]
      index [:likes]
      index [:visit_month, :user_id], :unique=>true
    end
  end
end
              Sequel.migration do
                change do
                  self << "SET search_path TO \"$user\", public"
                  self << "INSERT INTO \"schema_migrations\" (\"filename\") VALUES ('20170326105547_create_users.rb')"
self << "INSERT INTO \"schema_migrations\" (\"filename\") VALUES ('20170326110200_create_user_months.rb')"
self << "INSERT INTO \"schema_migrations\" (\"filename\") VALUES ('20170326110505_create_posts.rb')"
self << "INSERT INTO \"schema_migrations\" (\"filename\") VALUES ('20170326110517_create_likes.rb')"
self << "INSERT INTO \"schema_migrations\" (\"filename\") VALUES ('20170326110522_create_comments.rb')"
                end
              end
