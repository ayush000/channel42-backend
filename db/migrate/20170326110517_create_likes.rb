Sequel.migration do
  up do
    create_table :likes do
      primary_key :id
      foreign_key :user_id, :users, unique: false, type: 'varchar(255)'
      foreign_key :post_id, :posts, unique: false, type: 'varchar(255)'
      index [:user_id, :post_id], unique: true
    end
  end

  down do
    drop_table(:likes)
  end
end
