Sequel.migration do
  up do
    create_table :comments do
      String :id, primary_key: true
      foreign_key :user_id, :users, unique: false, type: 'varchar(255)'
      foreign_key :post_id, :posts, unique: false, type: 'varchar(255)'
      Timestamp :created_time
    end
  end
  down do
    drop_table(:comments)
  end  
end
