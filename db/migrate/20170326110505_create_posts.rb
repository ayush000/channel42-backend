Sequel.migration do
  up do
    create_table :posts do
      String :id, primary_key: true
      Timestamp :created_time
      Integer :shares
    end
  end
  down do
    drop_table(:posts)
  end
end
