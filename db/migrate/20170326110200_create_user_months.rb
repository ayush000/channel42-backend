Sequel.migration do
  up do
    create_table :user_month_visits do
      primary_key :id
      Date :visit_month
      foreign_key :user_id, :users, unique: false, type: 'varchar(255)'
      Integer :likes, null: false, default: 0
      Integer :comments, null: false, default: 0
      index [:likes]
      index [:comments]
      index [:visit_month, :user_id], unique: true
    end
  end
  down do
    drop_table(:user_month_visits)
  end
end
