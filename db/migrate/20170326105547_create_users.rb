Sequel.migration do
  up do
    create_table :users do
      String :id, primary_key: true
      Integer :likes, null: false, default: 0
      Integer :comments, null: false, default: 0
      index [:likes]
      index [:comments]
    end
  end
  down do
    drop_table(:users)
  end
end
