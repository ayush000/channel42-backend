Rails.application.routes.draw do
  root 'log#post_data'
  get '/user/one_time', to: 'user#one_time'
  get '/user/repeat', to: 'user#repeat'
  get '/user/repeat_n', to: 'user#repeat_n'
  get '/user/recurring_visits_monthly', to: 'user#recurring_visits_monthly'
  get '/user/bounce_rate_monthly', to: 'user#bounce_rate_monthly'
end
