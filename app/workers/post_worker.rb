class PostWorker
  include Sidekiq::Worker

  # Fetch all posts of a page and store them
  def perform(next_page_params)
    access_token = Sidekiq.redis { |r| r.get 'access_token' }
    # Get access token from redis
    client = Koala::Facebook::API.new(access_token)
    # Initialise if next page token is nil, otherwise use it
    posts =
      if next_page_params.nil?
        client.get_connection(STORE_ID, 'posts', limit: RESULT_LIMIT, fields: %w(id from{id} created_time shares))
      else
        next_page_params[1]['access_token'] = access_token
        client.get_page(next_page_params)
      end
    posts.each { |post| store_post(post) }
    PostWorker.perform_async(posts.next_page_params) unless posts.next_page_params.nil?
  end

  private

  def store_post(post)
    Post.unrestrict_primary_key
    if post['from']['id'] == STORE_ID
      shares = post.key?('shares') ? post['shares']['count'] : 0
      # Store post in database
      begin
        Post.new(created_time: post['created_time'], id: post['id'], shares: shares).save
      rescue Sequel::UniqueConstraintViolation
        p "Duplicate post #{post['id']}"
      end
      # Perform LikeWorker and CommentWorker jobs regardless of duplicate post
      LikeWorker.perform_async(post['id'], post['created_time'], nil)
      CommentWorker.perform_async(post['id'], nil)
    end
  end
end
