class CommentWorker
  include Sidekiq::Worker

  # Fetch all comments on a post and store them
  def perform(post_id, next_page_params)
    # Get access token from redis
    access_token = Sidekiq.redis { |r| r.get 'access_token' }
    client = Koala::Facebook::API.new(access_token)
    # Initialise if next page token is nil, otherwise use it
    comments =
      if next_page_params.nil?
        client.get_connection(post_id, 'comments', limit: RESULT_LIMIT)
      else
        next_page_params[1]['access_token'] = access_token
        client.get_page(next_page_params)
      end
    duplicates = 0
    comments.each do |comment|
      if comment_exists(comment['id'])
        duplicates += 1
      else
        store_comment(comment['id'], comment['from']['id'], post_id, comment['created_time'])
      end
    end
    p "Dup #{duplicates}"
    # If duplicates = 100 or next_page_params don't exist, execution stops
    # Duplicated are checked by checking unique constraint on comment_id
    if duplicates < 100 && !comments.next_page_params.nil?
      CommentWorker.perform_async(post_id, comments.next_page_params)
    end
  end

  private

  def comment_exists(comment_id)
    !Comment.where(id: comment_id).empty?
  end

  def store_comment(comment_id, user_id, post_id, created_at)
    visit_month = Time.parse(created_at).beginning_of_month.to_date
    tb_users = Sequel::Model.db[:users]
    tb_monthly = Sequel::Model.db[:user_month_visits]
    increment_comments = Sequel.deep_qualify(:users, Sequel.+(:comments, 1))
    increment_comments_monthly = Sequel.deep_qualify(:user_month_visits, Sequel.+(:comments, 1))
    tb_users.insert_conflict(target: :id, update: { comments: increment_comments }).insert(id: user_id, likes: 0, comments: 1)
    tb_monthly.insert_conflict(target: [:visit_month, :user_id], update: { comments: increment_comments_monthly }).insert(user_id: user_id, visit_month: visit_month, likes: 0, comments: 1)
    begin
      Comment.unrestrict_primary_key
      Comment.new(id: comment_id, user_id: user_id, post_id: post_id, created_time: created_at).save
    rescue Sequel::UniqueConstraintViolation
      p "Duplicate comment #{comment_id}"
    end
  end
end
