class LikeWorker
  include Sidekiq::Worker

  # Fetch all likes on a post and store them
  def perform(post_id, created_at, next_page_params)
    # Get access token from redis
    access_token = Sidekiq.redis { |r| r.get 'access_token' }
    client = Koala::Facebook::API.new(access_token)
    # Initialise if next page token is nil, otherwise use it
    likes =
      if next_page_params.nil?
        client.get_connection(post_id, 'likes', limit: RESULT_LIMIT)
      else
        next_page_params[1]['access_token'] = access_token
        client.get_page(next_page_params)
      end
    duplicates = 0
    likes.each do |like|
      if like_exists(post_id, like['id'])
        duplicates += 1
      else
        store_like(post_id, like['id'], created_at)
      end
    end
    p "Dup #{duplicates}"
    # If duplicates = 100 or next_page_params don't exist, execution stops
    # Duplicates are checked by checking unique index on post_id and user_id
    if duplicates < RESULT_LIMIT && !likes.next_page_params.nil?
      LikeWorker.perform_async(post_id, created_at, likes.next_page_params)
    end
  end

  private

  def like_exists(post_id, user_id)
    return !Like.where(user_id: user_id, post_id: post_id).empty?
  end

  def store_like(post_id, user_id, created_at)
    visit_month = Time.parse(created_at).beginning_of_month.to_date
    tb_users = Sequel::Model.db[:users]
    tb_monthly = Sequel::Model.db[:user_month_visits]
    increment_likes = Sequel.deep_qualify(:users, Sequel.+(:likes, 1))
    increment_likes_monthly = Sequel.deep_qualify(:user_month_visits, Sequel.+(:likes, 1))
    tb_users.insert_conflict(target: :id, update: { likes: increment_likes }).insert(id: user_id, likes: 1, comments: 0)
    tb_monthly.insert_conflict(target: [:visit_month, :user_id], update: { likes: increment_likes_monthly }).insert(user_id: user_id, visit_month: visit_month, likes: 1, comments: 0)
    begin
      Like.new(user_id: user_id, post_id: post_id).save
    rescue Sequel::UniqueConstraintViolation
      p "Duplicate user #{user_id}"
    end
  end
end
