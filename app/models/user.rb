class User < Sequel::Model
  one_to_many :likes
  one_to_many :comments
end
