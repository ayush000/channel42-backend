class UserController < ApplicationController
  # Users who visited once
  def one_time
    render json: { data: User.where('likes + comments = 1').count }
  end

  # Repeat users
  def repeat
    render json: { data: User.where('likes + comments > 1').count }
  end

  # Number of users who visited n times
  def repeat_n
    visits = Sequel.deep_qualify(:users, Sequel.+(:likes, :comments))
    visit_num_users = User.where('likes + comments BETWEEN 2 AND 100')
                      .group_and_count(visits.as(:visits))
                      .order(visits)
    render json: { data: visit_num_users }
  end

  # Monthly visits by recurring users and number of such users
  def recurring_visits_monthly
    data = Sequel::Model.db['SELECT SUM(likes + comments) AS visits, COUNT(1) AS users, visit_month FROM user_month_visits WHERE user_id IN (SELECT id FROM users WHERE likes + comments > 1) GROUP BY visit_month ORDER BY visit_month']
    render json: { data: data }
  end

  # Monthly ratio of one time visits / total visits
  def bounce_rate_monthly
    data = Sequel::Model.db['SELECT COUNT(CASE WHEN likes + comments = 1 THEN 1 ELSE NULL END) * 100 / SUM(likes + comments) AS bounce_rate, visit_month FROM user_month_visits GROUP BY visit_month ORDER BY visit_month']
    render json: { data: data }
  end
end
